﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject;
using Question1.Code;

namespace Question1.Interface
{
	class Program
	{
		static void Main(string[] args)
		{
			string filePath = args[0];
			string outputPath = args[1];
			int max = 0;
			int counter = 0;

			var file = new System.IO.StreamReader(filePath);

			var countline = file.ReadLine();
			max = int.Parse(countline);

			var Games = new Game[max];

			while (counter < max)
			{
				var line1 = file.ReadLine();
				var line2 = file.ReadLine();
				var line3 = file.ReadLine();
				var line4 = file.ReadLine();

				Games[counter] = new Game(counter + 1, line1, line2, line3, line4);

				file.ReadLine();
				counter++;
			}

			file.Close();

			foreach (var game in Games)
			{
				game.Result();
			}

			System.IO.File.WriteAllLines(outputPath, Games.Select(x => x.PrintResults()));



		}
	}
}
