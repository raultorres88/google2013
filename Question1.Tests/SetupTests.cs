﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Question1.Code;

namespace Question1.Tests
{
	[TestFixture]
	public class SetupTests
	{
		[Test]
		public void Setup()
		{
			var game = new Game(0, "....", "....", "....", "....");

			Assert.AreEqual('.', game.Board[0][0]);
			Assert.AreEqual('.', game.Board[3][3]);
		}

		[Test]
		public void CheckRowForWinner()
		{
			var game = new Game(0, "....", "....", "....", "xxxx");

			var result = game.CheckHorizontalForWinner('x', 2);
			Assert.False(false);
			
			result = game.CheckHorizontalForWinner('x', 3);
			Assert.True(result);

			var game2 = new Game(0, "x...", "x...", "x...", "x...");
			Assert.IsTrue(game2.CheckVerticalForWinner('x', 0));

			var game3 = new Game(0, "x...", ".x..", "..x.","...x");
			var game4 = new Game(0, "...x","..x.", ".x..", "x...");

			Assert.IsTrue(game3.CheckDiagnol1('x'));
			Assert.IsTrue(game4.CheckDiagnol2('x'));

			Assert.IsFalse(game3.CheckDiagnol2('x'));
			Assert.IsFalse(game4.CheckDiagnol1('x'));
		}


		[TestCase("....", "....", "....", "xxxx")]
		[TestCase("....", "....", "....", "xtxx")]
		[TestCase("....", "....", "xxxx","....")]
		[TestCase("....", "....", "xtxx", "....")]
		[TestCase("....", "xxxx", "....", "....")]
		[TestCase("....", "xtxx", "....", "....")]
		[TestCase("xxxx", "....", "....", "....")]
		[TestCase("xtxx", "....", "....", "....")]
		[TestCase("x...", "x...", "x...", "x...")]
		[TestCase("x...", "x...", "x...", "t...")]
		public void IsWinner(string r1, string r2, string r3, string r4)
		{
			var game = new Game(0, r1,r2,r3,r4);
			Assert.IsTrue(game.IsWinner('x'));
		}

		[TestCase("....", "....", "....", "oooo")]
		[TestCase("....", "....", "....", "otoo")]
		[TestCase("....", "....", "oooo", "....")]
		[TestCase("....", "....", "otoo", "....")]
		[TestCase("....", "oooo", "....", "....")]
		[TestCase("....", "otoo", "....", "....")]
		[TestCase("oooo", "....", "....", "....")]
		[TestCase("otoo", "....", "....", "....")]
		[TestCase("o...", "o...", "o...", "o...")]
		[TestCase("o...", "o...", "o...", "t...")]
		public void IsOWinner(string r1, string r2, string r3, string r4)
		{
			var game = new Game(0, r1, r2, r3, r4);
			Assert.IsTrue(game.IsWinner('o'));
		}


		[TestCase("X.O.", "X...", "X..O", "XO..", "X won")]
		[TestCase("X.O.", "X...", "X..O", "OOOO", "O won")]
		[TestCase("XOX.", "OX..", "....", "....", "Game has not completed")]
		[TestCase("XOXT", "XXOO", "OXOX", "XXOO", "Draw")]
		public void Result(string r1, string r2, string r3, string r4, string expected)
		{
			var game = new Game(0, r1, r2, r3, r4);
			var result =  game.Result();

			Assert.AreEqual(expected, result);

		}

	}
}
