﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Question1.Code
{
	public class Game
	{
		private readonly int _gameNumber;
		private readonly string[] rows;
		private char Freebie = 't';
		private char PlayerX = 'X';
		private char PlayerO = 'O';
		private char Unplayed = '.';
		private string Results;

		public char[][] Board;

		public Game(int gameNumber, string row1, string row2, string row3, string row4)
		{
			_gameNumber = gameNumber;
			rows = new string[4];
			Board = new char[4][];

			rows[0] = row1;
			rows[1] = row2;
			rows[2] = row3;
			rows[3] = row4;

			for (int i = 0; i < 4; i++)
			{
				Board[i] = new char[4];

				for(int x =0; x < 4; x++)
				{
					Board[i][x] = rows[i][x];
				}	
			}
		}

		public bool CheckHorizontalForWinner(char player, int row)
		{
			for (int i = 0; i < 4; i++)
			{
				if (Board[row][i] != player && Board[row][i] != Freebie)
				{
					return false;
				}
			}

			return true;
		}

		public bool CheckVerticalForWinner(char player, int column)
		{
			for (int i = 0; i < 4; i++)
			{
				if (Board[i][column] != player && Board[i][column] != Freebie)
				{
					return false;
				}
			}

			return true;
		}

		public bool CheckDiagnol1(char player)
		{
			for (int i = 0; i < 4; i++)
			{
				if (Board[i][i] != player && Board[i][i] != Freebie)
				{
					return false;
				}
			}

			return true;
		}

		public bool CheckDiagnol2(char player)
		{
			for (int i = 0, x = 3; i < 4; i++, x--)
			{
				if (Board[i][x] != player && Board[i][i] != Freebie)
				{
					return false;
				}
			}

			return true;
		}

		public bool IsWinner(char player)
		{
			for (int i = 0; i < 4; i++)
			{
				if (CheckHorizontalForWinner(player, i))
					return true;
				if (CheckVerticalForWinner(player, i))
					return true;
			}

			if (CheckDiagnol1(player))
				return true;
			if (CheckDiagnol2(player))
				return true;

			return false;
		}

		public bool IsOnGoing()
		{
			for (int i = 0; i < 4; i++)
			{
				for (int x = 0; x < 4; x++)
				{
					if (Board[i][x] == Unplayed)
					{
						return true;
					}

				}
			}

			return false;
		}

		public string Result()
		{
			if (IsWinner(PlayerX))
				Results = "X won";
			else if (IsWinner(PlayerO))
				Results = "O won";
			else if (IsOnGoing())
				Results = "Game has not completed";
			else
				Results = "Draw";

			return Results;
		}

		public string PrintResults()
		{
			return String.Format("Case #{0}: {1}", _gameNumber, Result());
		}
	}
}
