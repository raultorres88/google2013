﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject.Modules;

namespace Question1.Code
{
	public class DomainModule : NinjectModule
	{
		public override void Load()
		{
			Bind<Test>().ToSelf();
		}
	}
}
