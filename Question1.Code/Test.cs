﻿using Ninject.Extensions.Logging;

namespace Question1.Code
{
	public class Test
	{
		private readonly ILogger _logger;

		public Test(ILogger logger)
		{
			_logger = logger;
		}

		public void Run()
		{
			_logger.Trace("check this out");
		}
	}
}
